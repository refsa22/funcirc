# Report for IMT3602 Professional Programming
#### Members: Ole-Gustav Røed (olegustr)

## Group Discussion:
### Strengths and Weaknesses of F#
F# is a functional programming language on the .NET technology stack. As such it can cooperate with other languages using the .NET runtime. This is in my view the biggest strength of using F# as it can easily be used together with C# allowing you to choose the best language for the task. 

F# is not a pure functional programming language though and as such it does allow the use of imperative and OOP principles. This can be viewed as both a strength and a weakness but looking at it from the point of learning functional programming it can be bad. Mostly because one can choose to go for the "easy way" out and not force the functional mindset.

Biggest downside to F# is that it is not very widespread in use. As such the availability for tools and the maturaty of certain aspects of the language is not as far ahead as say C# or Haskell. This can make it a frustrating experience as the availability of document generators and static analysis tools are limited. Couple this with the rather hard to read syntax of the code and compiler messages and it can be a tough language to get started with.

One of the better aspects of F# is that it is functional first. This means that it supports the use of a functional mindset which can in many situations lead to shorter and more concise code. Advanced pattern matching, currying and the ability to pipe the output of one function into the input of another lets the user string together pieces of codes in varying ways. The language does not support generic parameters but has a strong type inference allowing the language to generate the necessary inner workings without the user explicitly specifying the type. As such one can create a function to add two numbers together without strictly saying they are for floats or doubles without having to make use of polymorphism. 

F# also has great support for concurrency without the additional work of manually creating threads. It has agents similar to Clojure, namely the MailboxProcessor. This is a concurrent message inbox that allows the user to asynchronously write message to it that it can digest at its own pace. There is no need to create semaphores or resource locks, everything is managed internally by the MailboxProcessor architecture. All of this is made to work with .NETs Thread Pools which means that they are properly created and destroyed allowing you to spin up thousands of instances with minimal performance impact.

For many the lack of explicitly specifying the type is a huge turn-off for a language. Add on top the high sensitivity to whitespace and F# is for many a cumbersome and abstract language to use. This is an area I mostly agree with even though it does have many positive effects. In the end it's more about picking the right tool for the job.

### Use of Libraries
Use of external libraries was to be kept to a minimum. Considering the scope of this project and the size of the integrated functionality from .NET there shouldn't be much reason to add in a bunch of external dependencies. This ensures that the general project can stay concise and avoid including a dependency that is not supported on specific platforms. 

Libraries are often very useful and can greatly improve the speed of which a project can be created. On the contrary they can add in unneded secondary dependencies and in general be unreliable. It greatly increase the amount of effort one has to spend to make sure nothing in the included library is faulty and can lead to secondary errors outside of ones control. On top of this if you only need a smaller subset of the library it might not be worth it to take on the additional final filesize of the binary and/or any accompanying library files. Often it might be better to implement that subset into the project if say the functionality is something as small as a method to clamp a number inside a range. 

On the other end there is also the age old "don't reinvent the wheel" and there certainly needs to be some thought put into what type of libraries to use. Recreating a whole network stack for the purpose of running a small web-service might be outside the scope of most projects. The little performance one can gain from using an in-house solution could be worth it in time-critical systems, but for the vast majority of projects it's better to use and contribute to already existing projects.

For this project there is only one third-party library included, which is FParsec. This is strictly not necessary to make the project work but was included in a great need to organize the message parsing logic. On top of this it was on my list of things to explore with F#, so it was a great place to make use of it. FParsec can greatly reduce the amount of logic while still being concise and clear about what it does. It is a character parsing library akin to Regex, but where Regex is cumbersome to manage outside of simple parsing statements, FParsec can be split up into many smaller segments and put together into a complete parser later on. Instead of writing ``` @"^foo(bar)" ``` one can instead express it by writing ``` pstring "foo" >>. pstring "bar" ```. This is a very simple example but avoids the use of generic capture groups and clearly divides the parsing logic into several pieces. On top of this there is no longer a need to rely on the internal data structure of Regex parsers in order to get the result you want.

### Process and Communication
The whole project was utilizing the functionality available at GitLab. This ensured that all communication about the project stayed in close contact with the project. Segmenting bug reports and issues accross multiple platforms can with certain project and team sizes be appropriate but this project was more aimed at a smaller, more focused group of people. 

GitLab does not have a proper instant messaging system and as such something like Discord is more relevant for these use cases. Smaller segments of information about the meta-state and progress of the project is better fit for this communication channel. It was still important to make sure that all decisions were recorded in a more structured way so putting tasks and changes into the issue board was important.

### Version Control, Tickets and Management
The whole project is managed through git making heavy use of the issue tracker and accompanying smart commits. The master branch was always kept as the end point and it was made sure that no active development was done here. This is important in order to have good knowledge about the current state of the project and makes sure the code there is as ready for production as possible. Cluttering up the main branch is a good way to creating unecessary issues for everyone involved in the project.

All changes were done with branches where the name was relevant to the features and/or changes to be made with it. Within the development on a branch almost all commits were linked to the issue tracker with smart commits. This ensured there is a clear pathway to who made a change and makes it clear what changes went down after a merge request is made. After all relevant changes was done to a branch and the required test cases was ready it could be merged with the main branch. Merging had to make sure that any new or old test cases was completed successfully using CI to ensure no mistakes were brought over to the main branch.

Milestones were used to a limited degree and is certainly something that should have been used more carefully. This is a good way to segment the project into smaller pieces and in the long run give a better overview of the projects state at certain intervals of the development. In addition it is possible to link issues to milestones which would give them more context. 

Some attempts were made to keep up with time estimates but was ultimately underprioritized for the project as a whole. For a project this size it would be more useful as an interesting metric for the progress of the development rather than something that would help the overall development cycle. It is still a really good way to practice how to estimate the time to implement features even though one might spend a lot of time managing the different smart commits and making sure they are applied.

### Continous Integration / Continous Deployment
This project made heavy use of CI in order to make better use of TDD. There was a lot of focus on writing tests to fullfill the requirements before the actual codebase was written. As such it was really handy to have an automated system in place to run these tests whenever something was pushed on the main branch. This ensured that there was no need to run every single test whenever a new feature was added and acted as a backup to avoid merging broken code onto the master branch.

There was also some experimentation with CD in order to generate documentation and publish them to Gitlab Pages. Since this project was focused on developing an API for an IRC Client there was a huge bonus by having an automated system in place to generate the relevant documentation. F# didn't seem to be fully supported by DocFX so there were some issues in order to get it properly setup for use.

For project down the line it would probably be better to set up a proper docker image to run tests inside. Currently it makes use of a local gitlab-runner using a shell instance. This means that a personal development computer has to be active and connected to the gitlab-runner service for tests to be ran. As such it means that it can easily be broken and developers will have to wait until that specific computer is ready again. Part of why it was done this way was because it required a Pro version of Windows which I didn't have at the time. Spending the extra time reinstalling Windows might have been worth it for the experience of setting up docker images but were ultimately regarded as not important.

### Programming Style Guide
The main focus of programming style guide is the official Microsoft guidelines. Both the [F#](https://docs.microsoft.com/en-us/dotnet/fsharp/style-guide/conventions) and [C#](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/coding-conventions) coding conventions were taken in to consideration during the project. 

Since F# is a whitespace sensitive language there are some areas that required more freedom. This might also be related to my experience in F# as a language and as such some areas of the code might not be fully up to standard. Other than the general formatting rules the naming conventions was always followed to ensure that the project has consistency all throughout its codebase.

All comments were created using the XML comment format. This was both to properly support IDE integration and the use of documentation generator such as DocFX and Doxygen. Any publically facing function, method or memeber field was to have at least a summary tag and any parameters and return values should also be tagged.

### Use of Code Review
Code review was used in order to gauge the overall appearance and functionality of the code. Biggest gain from this was to avoid really bad code smells or to catch some more common mistakes made. To get proper use out of the code reviews they had to be weaved into the general development process without being in the way of general development. This meant that they were done after milestones were completed and recorded in a document for future review. After the code review was done it was sorted into categories and priority and the most relevant pieces was added to the issue tracker based on their urgency.

Adding them to the issue tracker was a good way to make them visible to the overall project management. It forced them to be considered when chosing the next tasks to complete and didn't hide them away in a file somewhere in the project. If a code review produced a lot of issues a branch was created to deal with them directly. In this way they could be linked back to a code review summary file to give more context to why the changes were necessary.

## Individual Discussion:

### Good Code
```fsharp
/// <summary> Splits the individual tags by the = character if it is there </summary>
let pSplitTag: Parser<_> = 
    let keyParser =  manyChars (noneOf "=;")
    let valueParser = pstring "=" >>. manyChars (noneOf ";") <|>% ""
    optional (skipString ";") >>. (keyParser .>>. valueParser)

/// <summary> parses the tags only string into (key, value) list </summary>
let splitAllTags: Parser<_> =
    many1 (notEmpty pSplitTag) <|> (pstring "" >>. preturn [])
```

Of course, all of this is what I would consider good code, and I might be completely wrong in my assessment. Trying to find a piece of code that is archetypical good code in my own project was hard and the question could be read as "perfect code". There might not exist any piece of code that is perfect so I ended up with a segment of code from the project I'd say can be justified as good. 

As an example of good code I chose to go with code from the message parsing logic. One part of me says that this isn't a really solid example of good code but it still shows the areas that are important. Main problem is that it makes use of a library that uses custom operators. As such it can be hard to properly understand what's going on without some prior knowledge of FParsec.

Other than that the code goes straight to the point. Function names mostly explain what their internals are supposed to do. Instead of going the route where everything is compressed into one line I rather made the choice to split it over several lines. This should make it easier to read since the core concept of the parsing logic is rather abstract. 
I tried to make sure that each line could be read from left to right to hopefully take advantage of something resembling a sentence. So even if you have no experience with FParsec before it should still somewhat make sense as to what the code is doing.

### Bad Code
```fsharp
/// <summary>
/// Takes the list of tags extracted in messageSplit and creates a list of Tag Records from them
/// </summary>
let parseTags (tagSplit: string list option): Tag list option =
    match tagSplit with
    | None -> None
    | Some tagSplit ->
        Some 
            [
                for tag in tagSplit -> 
                    arrayRemove (tag.Split('=')) stringIsEmpty
                    |> fun kvp -> 
                        match kvp.Length with
                        | IsTagKeyOnly  -> {Key = kvp.[0];   Value = None}
                        | IsTagKeyValue -> {Key = kvp.[0];   Value = Some kvp.[1]}
                        | IsTagNone     -> {Key = "FAILURE"; Value = None}
            ]
```

The code above is a good example of what I would call bad code. The lambda function in the inner for..in loop could be moved out into it's own function to reduce the amount of horizontality in the code. The last part of the inner active pattern also badly reflects the state of the function when the parsing was unsuccessful. This could make it hard for a user the actually gauge the status of the parser while having a certain chance to overlap with actual tag names. It's an unstable way to report a status from the function and has a high likelyhood of crossover with other functionality. The XML comment above the function also does not reflect the eventual failure state making it undescriptive and could lead to confusion and bugs down the line.

I'm sure I could find a whole range of other examples of bad code in this project and my other projects and a lot of examples of terrible code. This part of my codebase I what was ingrained into my mind as being horrible and as such it sort of automatically got brought up into this report. 

### Example of Refactoring
One of the initial issues with the project was to find a good way to handle the message parsing. In essence it's not all that complex but involves a lot of sub-steps with some areas requires handling 6-7 different scenarios. The part of my project that I will talk about refactoring is the contents of MessageParserInternals.fsx. This is where the incoming messages were split up into it's three constitutent parts. Instead of removing the old code completely it was left in so I had a reference point to do a comparison with allowing me to utilize the strategy pattern to switch back and forth. I already had test cases setup for the old parser logic and as such it ensured that the refactoring ended up with code that had the same functionality.

The example covers the entierty of MessageParserInternals into MessageParserInternalsV2 but I'll just include a small snippet of the refactor here. Specifically the part of the parser that looks for the source part and splits it into nick, user and host.

Example of code before refactoring:
```fsharp
let parseSource (sourceString: string option): Source option =
    if sourceString = None then None
    else

    let sourceString = sourceString.Value
    let nickSplit    = arrayRemove (sourceString.Split('!')) stringIsEmpty
    let hostSplit    = arrayRemove (sourceString.Split('@')) stringIsEmpty

    match (nickSplit, hostSplit, (sourceString.IndexOf('.') <> -1)) with
    | IsHost         -> Some {Nick = None;               User = None;                           Host = Some sourceString}
    | IsNick         -> Some {Nick = Some sourceString;  User = None;                           Host = None}
    | IsUserHost     -> Some {Nick = None;               User = Some (hostSplit.[0].Trim('!')); Host = Some (hostSplit.[1])}
    | IsNickUser     -> Some {Nick = Some nickSplit.[0]; User = Some nickSplit.[1];             Host = None}
    | IsNickHost     -> Some {Nick = Some hostSplit.[0]; User = None;                           Host = Some hostSplit.[1]}
    | IsNickUserHost -> 
        Some
            {
                Nick = Some nickSplit.[0];
                User = Some (nickSplit.[1].Replace(hostSplit.[1], "").Trim('@'));
                Host = Some hostSplit.[1];
            }
```

This code shows a lot of minor issues related to how I initially solved the parsing problem. There is one main cause for a rather messy code structure; it was my first segment of F# code in a while and as such I didn't follow the structure of functional programming all that well. It should still be readable and you should be able to get an understanding of what the code does, but it's a rather messy approach. Mainly the overuse of whitespace and how I made use of the tuple to do the pattern matching leaves a lot of open questions as to how it processes the message. The code uses a lot of space horizontally and as such it gets hard to gauge what each line actually does when first looking at it. 

This solution also used a short bit of regex in order to retreive the Source part from the entire IRC message: 
```
fsharp let sourceRegex  = @"^(:[\S.]+)"
```

Regex isn't necessarily a bad option in order to do this sort of message parsing, but it is not able to actually do any additional processing. On top of this it quickly gets unreadable once look-ahead functionality is used and this leads into future problems when or if it needs to be changed. This lead to a decision where the choice was between refactoring and using the same methodology or start with an entierly new solution. 

My choice ended up with using FParsec in order to solve the problem. Using it meant that I had to involve an additional library to solve the problem, but it also ment I would end up with a more customizable and readable solution. As such the code that does the same as the one above was converted to this:

```fsharp
/// <summary> Parses the nick part of a source segment if it exists </summary>
let nickParser: Parser<_> = 
    optional (skipString ":") >>. (manyChars (noneOf "@!./")) .>>? notFollowedBy (anyOf "./")

/// <summary> Parses the user part of a source segment if it exists </summary>
let userParser: Parser<_> = 
    pstring "!" >>. manyCharsTill (noneOf "") (lookAhead (pstring "@"))

/// <summary> Parses the host part of a source segment if it exists </summary>
let hostParser: Parser<_> = 
    optional (skipString "@" <|> skipString ":") >>. manyChars (noneOf "") .>> eof

/// <summary> Splits the source into (nick, user, hostname) </summary>
let splitSource: Parser<_> =
    pipe3 
        ( attempt nickParser |> optionalEmptyString ) // Nick
        ( attempt userParser |> optionalEmptyString ) // User
        ( hostParser ) // Host
        ( fun a b c -> (a, b, c) )
```

Using FParsec greatly reduced the need to organize the code using whitespace and arbitrary tabbing. On top of this it was very easy to split the parsing into three separate parts instead of doing everything in one go. This means that testing the code was easier and could be more specialized with less mocking. It is now possible to read the parser like a sentence and less like a long list of choices. Of course, this refactor also means that anyone who reads this code needs to have some knowledge of how either FParsec or Parsec works in addition to understanding how F# allows you to create custom operators. It looks weird at first but have allowed me to come back to this code months later and still be able to see what the code does quickly. 

Another refactor that could be done to MessageParserInternalsV2 is to reduce the amount of comments. There is a couple of superfluous comments in order to organize the code that only exist because of the lack of the #region declarative and whitespace noise in F#. This might not help much with organizing the code at all and can look like visual clutter to a person that didn't write the code, defeating its purpose. This part of the code will not be exposed to the public API either, so the value of comments is limited in that sense. I'm sure some arguments could also be made as to how I made use of FParsec as I recognize some areas might go two steps ahead of the solution and then one back.

### Professionalism in Programming
"Professionalism" is sort of a vague term that doesn't really explain what it tries to accomplish. It is about how you conduct yourself towards your own code, the code others write and how you are able to communicate the work you have done are are going to do. It is also about the methodology and practices used while programming in order to create code that is concise, safe and easy to read for other programmers. I see that the issue of professionalism in programming can be split into several categories that are all affected by the teams size and agreements.

#### Communication
First of all I would say one part of professionalism in programming is how you communicate your thoughts and ideas to others. It is important that everyone uses the same frame of reference which involves software programming paradigms, language specific quirks and tools used to manage the project. 

Programming paradigms is a good way to explain how you want to solve a problem. It is a tested solution to a problem and often has a rigid structure that should make sense in the context of the problem. This can make the discussion around the solution shorter and avoid having to go into implementation specific details in order to communicate the overall idea. 

Next part of communication is language specific quirks and how they interact with the overall problem to be solved. Certain programming languages might already have the required components to create a solution for a paradigm and as such it's important that this is explained. If a language has good support for async programming it means that you don't really have to go into specifics of how to make an async structure work in comparison to a language with less support for it.

These two points forms the basis of how to communciate general problems surrounding a code base. This would then be put into practice where the problem is discussed as a group before the solution is implemented. It's better to have this discussion beforehand so one can be sure that the entire process of a codebase is put into consideration. A single person won't have a perfect understanding of everything and it helps to bring new people up to speed. Doing something twice or making a solution that breaks one aspect of the program can and will be a waste of time and can be easily avoided with proper communication.

Last but not least it's important to communicate through the use of tools. Keeping people updated on an issue even if they weren't present when the solution was derived is important to avoid clashing between the team members work. Having a central place to discuss features and changes also allows updates and new ideas to come forth more naturally. Platforms such as Jira is a great way to do this indirect communication and makes sure that not everything is spread through word of mouth. Keeping people synchronized should avoid common problems that can arise when just using verbal communication. 

#### Code and Code Structure
When writing code it's important that you don't set yourself up for a lot of technical debt. Larger teams should have decided on certain areas that makes up the essence of what programming is. That means that a choice have to be made for naming conventions, commit messages, formatting rules, comments, etc. Part of it is creating these rules but the main issue is to actually follow them. This is what I feel is the most important aspect of professional programming and is where the biggest impact can be made in order to create code that is maintainable and stable.

A piece of code is not worth much without some way to tell that it does what it's supposed to. This means that code has to be written in a way such that it's easy to test on an individual level. This has several positive benefits since having to create functions and classes that are easy to test should also lead to code that is easy to read. Having a large web of interconnected components can often be hard to avoid depending on the issue at hand, but I see this as an area that is worth the extra time. Having a rigid testing suite for the codebase requires a lot of extra work that might not pay off until several months or years down the line. It acts as a safety barrier to avoid pushing broken changes and might save the project from reimplementing a bug it previously solved. 

There is also the term of "self-documenting code" which is a bit of a misnomer and often spread as gospel without actually going into details. Having code that explains itself is all well and good, but it's not going to help anyone write better code and often falls in the same category as "Keep it simple, stupid". How I see this statement is that it's directly related to naming conventions and formatting rules. Avoid cutting down method- and variable-names just to save a few characters and make sure to not force a solution to a problem. Write comments on publically facing methods to explain quirks and specifics and not to reiterate on what the method name and it's argument themselves explain. Keep methods short and don't rewrite code that is already implemented to avoid segmentation in the code base. All of these are very situational and might be hard to follow with less experience with a code base. As such this is linked with communication and it's important to express problems in a group discussion to unroot larger, more technical problems.

Another case to be made is to learn and understand existing code in the project. When solving a problem one should ensure to make use of existing code before implementing anything new. Creating a new method that is 90% identical to another usually means that the code can be refactored and split up into smaller components and put together to fullfill the needed functionality. This is better than just implementing a separate function since it reduces the amount of code segmentation in the project.

#### Credits where credit is due
The last part of how I see professionalism is how you conduct yourself towards code written by other people. If you make use of a library or a piece of code made by someone else it should be documented where it's from. Giving credit for other peoples work is important and shows that you respect the time and effort someone made, even if the code used might seem insignificant. 

#### Conculsion
Many of the thoughts above are a collections of ideas I've found to make sense when reading about professionalism and how one should conduct oneself when programming. My experience in large teams or professional situations are limited so I might not hit the mark in some areas. In the end I'd say that professionalism in programming is much the same as professionalism in any other line of work. It's about respect and laying the groundwork for yourself and others to have a good way to communicate and understand the goal of a project.