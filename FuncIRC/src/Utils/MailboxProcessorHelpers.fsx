namespace FuncIRC

open System

module internal MailboxProcessorHelpers =

    /// <summary>
    /// Creates a MailboxProcessor object with type 'T, issues the updateDelegate whenever new information is received
    /// </summary>
    let mailboxProcessorFactory<'T> (updateDelegate: 'T -> unit) =
        MailboxProcessor<'T>.Start
            (fun update ->
                let rec loop() = async {
                    let! newInfo = update.Receive()

                    try updateDelegate newInfo
                    with | _ -> ()

                    return! loop()
                }
                loop()
            )