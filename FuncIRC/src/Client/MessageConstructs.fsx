#load "../Utils/GeneralHelpers.fsx"
#load "../IRC/Types/MessageTypes.fsx"

namespace FuncIRC

open MessageTypes
open GeneralHelpers

module internal MessageConstructs =
//#region Internal Message Constructs
    /// Creates the intial CAP message sent on registration
    let internal capStartMessage = 
        { Tags = None; Source = None; Verb = Some (Verb "CAP"); Params  = Some (toParameters [| "LS"; "302" |]) }

    let internal capReqMessage (features: string array) =
        { Tags = None; Source = None; Verb = Some (Verb "CAP"); Params  = Some (toParameters (Array.append [|"REQ"|] features )) }

    let internal capEndMessage =
        { Tags = None; Source = None; Verb = Some (Verb "CAP"); Params  = Some (toParameters [| "END" |]) }

    /// Creates a PASS message with the given pass string
    let internal passMessage pass = 
        { Tags = None; Source = None; Verb = Some (Verb "PASS"); Params = Some (toParameters [| pass |]) }

    /// Creates a NICK message with the given nick string
    let internal nickMessage nick = 
        { Tags = None; Source = None; Verb = Some (Verb "NICK"); Params = Some (toParameters [| nick |]) }

    /// Creates a USER message with the given user and realName strings
    let internal userMessage user realName =
        { Tags = None; Source = None; Verb = Some (Verb "USER"); Params = Some (toParameters [| user; "0"; "*"; ":" + realName |]) }

    /// Creates an AWAY message with the given awayMessage string
    let internal createAwayMessage awayMessage =
        { Tags = None; Source = None; Verb = Some (Verb "AWAY"); Params = Some (toParameters [| awayMessage |]) }

    /// Creates a QUIT message with the given quitMessage string
    let internal createQuitMessage quitMessage =
        { Tags = None; Source = None; Verb = Some (Verb "QUIT"); Params = Some (toParameters [| quitMessage |]) }

    /// Creates a KICK message with the given kickTarget and kickMessage strings
    let internal createKickMessage kickTarget kickMessage =
        { Tags = None; Source = None; Verb = Some (Verb "KICK"); Params = Some (toParameters [| kickTarget; ":" + kickMessage |]) }

    /// Creates a TOPIC message with the given topic string
    let internal createTopicMessage topic =
        { Tags = None; Source = None; Verb = Some (Verb "TOPIC"); Params = Some (toParameters [| topic |]) }

    /// Creates a JOIN message on the given channel
    let internal createJoinChannelMessage channel = { Tags = None; Source = None; Verb = Some (Verb "JOIN"); Params = Some (toParameters [|channel|]) }

    /// Creates a PRIVMSG message with the given message string on the given channel
    let internal createChannelPrivMessage message channel = { Tags = None; Source = None; Verb = Some (Verb "PRIVMSG"); Params = Some (toParameters [|channel; ":" + message|]) }
//#endregion