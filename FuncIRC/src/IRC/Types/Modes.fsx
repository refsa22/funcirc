namespace FuncIRC

/// All modes that users and channels can have
module Modes =

    /// <summary> All usermodes that can be supported by client/server </summary>
    type UserMode =
        | INVISIBLE
        | OPER
        | LOCAL_OPER
        | REGISTERED
        | WALLOPS

    /// <summary> All channel modes that can be supported by client/server </summary>
    type ChannelMode =
        | BAN
        | EXCEPTION
        | CLIENT_LIMIT
        | INVITE_ONLY
        | INVITE_EXCEPTION
        | KEY
        | MODERATED
        | SECRET
        | PROTECTED
        | NO_EXTERNAL_MESSAGE

    /// <summary> All user prefixes supported by client/server </summary>
    type ChannelMembershipPrefix =
        | FOUNDER
        | PROTECTED
        | OPERATOR
        | HALFOP
        | VOICE

    /// <summary> Maps channel membership prefixes to string values </summary>
    let channelMembershipPrefixMap =
        [ ChannelMembershipPrefix.FOUNDER, [ "+q" ]
          ChannelMembershipPrefix.PROTECTED, [ "+a" ]
          ChannelMembershipPrefix.OPERATOR, [ "+o"; "%" ]
          ChannelMembershipPrefix.HALFOP, [ "+h"; "%" ]
          ChannelMembershipPrefix.VOICE, [ "+v"; "+" ] ]
        |> Map.ofList

    /// <summary> Maps channel modes to string values </summary>
    let channelModeMap =
        [ ChannelMode.BAN, "+b"
          ChannelMode.EXCEPTION, "+e"
          ChannelMode.CLIENT_LIMIT, "+l"
          ChannelMode.INVITE_ONLY, "+i"
          ChannelMode.INVITE_EXCEPTION, "+I"
          ChannelMode.KEY, "+k"
          ChannelMode.MODERATED, "+m"
          ChannelMode.SECRET, "+s"
          ChannelMode.PROTECTED, "+t"
          ChannelMode.NO_EXTERNAL_MESSAGE, "+n" ]
        |> Map.ofList

    /// <summary> maps user modes to string values </summary>
    let userModeMap =
        [ UserMode.INVISIBLE, "+i"
          UserMode.OPER, "+o"
          UserMode.LOCAL_OPER, "+O"
          UserMode.REGISTERED, "+r"
          UserMode.WALLOPS, "+w" ]
        |> Map.ofList
